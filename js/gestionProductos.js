var productosObtenidos;


function getProductos(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange=function(){
    if (this.readyState==4 || this.status==200) {//estatus 4 = respondido estatus 200 todo bien
      //console.table(JSON.parse(request.responseText).value);//de texto a JSON para operar
        productosObtenidos = request.responseText;
        procesarProductos();
    }
  }

  request.open("GET",url,true);
  request.send();
}
function procesarProductos() {
  var JSONproductos =JSON.parse(productosObtenidos);
  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-hover");
    //alert(JSONproductos.value[0].ProductName);
    for (var i = 0; i < JSONproductos.value.length; i++) {
      console.log(JSONproductos.value[i].ProductName);
      var nuevaFila = document.createElement("tr");

      var columnaNombre = document.createElement("td");
      columnaNombre.innerText =JSONproductos.value[i].ProductName;
      var columnaPrecio = document.createElement("td");
      columnaPrecio.innerText =JSONproductos.value[i].UnitPrice;
      var columnaStock = document.createElement("td");
      columnaStock.innerText =JSONproductos.value[i].UnitsInStock;
      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaPrecio);
      nuevaFila.appendChild(columnaStock);

      tbody.appendChild(nuevaFila);
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}
